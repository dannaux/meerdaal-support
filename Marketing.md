# Meerdaal App

De Meerdaal app is bedoeld voor iedereen die de leden van Meerdaal wil volgen. 

De app bestaat uit 2 delen: een club specifiek gedeelte en een VTTL gedeelte

## Club

Met de app kan je, als speler/trainee:

- kijken wanneer je volgende trainingen zijn
- aangeven of je aanwezig zal zijn op de training
- kijken wie er nog komt naar de training

Als trainer kan je:
- ingeven wie er aanwezig was op de training
- een nieuwe trainee ingeven, of iemand verwijderen die niet meer meetraint
- de training doorgeven aan een andere trainer
- een training afzeggen
- bekijken wie hoeveel keer naar de training is gekomen

Als speler kan je:
- aangeven wanneer je beschikbaar bent om opgesteld te worden voor een wedstrijd

## VTTL

Met de app kan je

- de resultaten bekijken van alle VTTL spelers
- spelers als favorieten kiezen waarvan je de resultaten in een oogopslag kan bekijken
- de resultaten opvolgen van de Meerdaal teams
- de lijst van toernooien raadplegen en eenvoudig inschrijven via de VTTL website


