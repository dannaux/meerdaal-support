# Account en gegevens verwijderen van de Meerdaal app
Vanaf versie 0.5.x kan je zelf je account verwijderen. Alle profiel gegevens die je via de app hebt ngegeven (avatar, settings) zullen verwijderd worden. 

Om je account te verwijderen:
1. Open je profiel door op de start pagina rechtsboven op je avatar of initialen (indien je nog geen avatar ingesteld hebt) te klikken.
2. Klik onderaan de pagina op de knop 'Verwijder account'. Scroll naar beneden indien nodig. 
3. Klik bevestigen wanneer je de account wil verwijderen. 

Je account wordt verwijderd en je wordt naar de login pagina genavigeerd. 