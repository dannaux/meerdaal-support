# Privacybeleid
Ingangsdatum: December 04, 2018

Meerdaal ("ons", "wij" of "onze") beheert de Meerdaal mobiele App ("hierna genoemd Dienst").

Deze pagina bevat informatie over ons beleid met betrekking tot de verzameling, het gebruik en de openbaarmaking van uw persoonsgegevens wanneer u onze Dienst gebruikt en de keuzes die u hebt met betrekking tot die gegevens. Privacybeleid via Privacy Policies.

Wij gebruiken uw gegevens om de Dienst te leveren en te verbeteren. Wanneer u de Dienst gebruikt, gaat u akkoord met de verzameling en het gebruik van informatie in overeenstemming met dit beleid. Tenzij anders bepaald in dit Privacybeleid heeft de terminologie die wordt gebruikt in dit Privacybeleid dezelfde betekenis als in onze Algemene voorwaarden.

## Definities
- Dienst: onder dienst verstaan wij de Meerdaal mobiele App beheerd door Meerdaal

- Gebruiksgegevens: onder gebruiksgegevens verstaan wij automatisch verzamelde gegevens die worden gegenereerd door het gebruik van de Dienst of van de infrastructuur van de Dienst zelf (bijvoorbeeld, de duur van het bezoek aan een pagina).

 - Gebruiksgegevens: onder gebruiksgegevens verstaan wij automatisch verzamelde gegevens die worden gegenereerd door het gebruik van de Dienst of van de infrastructuur van de Dienst zelf (bijvoorbeeld, de duur van het bezoek aan een pagina).

 - Cookies: cookies zijn informatiebestandjes die worden opgeslagen op uw apparaat (computer of mobiele apparaat).

## Gegevensverzameling en gebruik
Wij verzamelen verschillende soorten gegevens voor uiteenlopende doeleinden om onze Dienst aan u te kunnen leveren en om hem te verbeteren.

### Soorten gegevens die worden verzameld
#### Persoonsgegevens
Wanneer u onze Dienst gebruikt, kunnen wij u vragen ons bepaalde persoonlijk identificeerbare informatie te verstrekken die kan worden gebruikt om contact op te nemen met u of om u te identificeren ("Persoonsgegevens"). Deze persoonlijk identificeerbare informatie kan omvatten maar is niet beperkt tot:

- Voor- en achternaam
- Cookies en Gebruiksgegevens

#### Gebruiksgegevens
Wanneer u toegang krijgt tot de Dienst met of via een mobiele apparaat kunnen wij bepaalde gegevens automatisch verzamelen, inclusief, maar niet beperkt tot, het mobiele apparaat dat u gebruikt, de unieke ID van uw mobiele apparaat, het IP-adres van uw mobiele apparaat, uw mobiel besturingssysteem, de mobiele internetbrowser die u gebruikt, de unieke apparaat-ID en andere diagnostische gegevens ("Gebruiksgegevens").

#### Tracking & cookiegegevens
Wij gebruiken cookies en soortgelijke volgtechnologieÃ«n om de activiteit op onze Dienst te volgen en we bewaren bepaalde informatie.

Cookies zijn bestanden met een kleine hoeveelheid gegevens die een anonieme unieke ID kunnen bevatten. Cookies worden van een website verzonden naar uw browser en opgeslagen op uw apparaat. Er worden ook andere volgtechnologieÃ«n gebruikt zoals beacons, tags en scripts om gegevens te verzamelen en te volgen en om onze Dienst te verbeteren en te analyseren.

U kunt uw browser instellen om alle cookies te weigeren of om aan te geven wanneer een cookie wordt verzonden. Maar als u geen cookies aanvaardt, kunt u mogelijk niet alle functies van onze Dienst gebruiken. You can learn more how to manage cookies in the Browser Cookies Guide.

Voorbeelden van cookies die wij gebruiken:

- Sessiecookies. Wij gebruiken Sessiecookies om onze Dienst te beheren.
- Voorkeurcookies. Wij gebruiken Voorkeurcookies om uw voorkeuren en uiteenlopende instellingen bij te houden.
- Veiligheidscookies. Wij gebruiken Veiligheidscookies voor veiligheidsdoeleinden.

### Gebruik van gegevens
Meerdaal gebruikt de verzamelde gegevens voor uiteenlopende doeleinden:

- Om onze Dienst te leveren en te onderhouden
- Om u wijzigingen in onze Dienst te melden
- Om u de mogelijkheid te bieden om, indien gewenst, deel te nemen aan de interactieve functies van onze Dienst
- Om onze klanten steun te verlenen
- Om analyse- of waardevolle gegevens te verzamelen die we kunnen toepassen om onze Dienst te verbeteren
- Om toezicht te houden op het gebruik van onze Dienst
- Om technische problemen te detecteren, te voorkomen en te behandelen
- Om u nieuws, speciale aanbiedingen en algemene informatie te bieden over onze goederen, diensten en evenementen die gelijkaardig zijn aan wat u in het verleden al gekocht hebt of waar u informatie over hebt gevraagd, tenzij u hebt aangegeven dat u dergelijke informatie niet wenst te ontvangen.

### Overdracht van gegevens
Uw gegevens, inclusief Persoonsgegevens, kunnen worden overgedragen naar â€” en bewaard op â€” computers die zich buiten het rechtsgebied van uw provincie, land of een andere overheidsinstantie bevinden waar de wetgeving inzake gegevensbescherming kan verschillen van de wetgeving in uw rechtsgebied.

Let erop dat, als u zich buiten Belgium bevindt en u ons gegevens verstrekt, wij deze gegevens, inclusief Persoonsgegevens, overdragen naar Belgium en ze daar verwerken.

Uw instemming met dit Privacybeleid gevolgd door uw indiening van dergelijke gegevens geven aan dat u akkoord gaat met die overdracht.

Meerdaal zal alle redelijkerwijs noodzakelijke stappen ondernemen om ervoor te zorgen dat uw gegevens veilig en in overeenstemming met dit Privacybeleid worden behandeld en dat uw Persoonsgegevens nooit worden overgedragen aan een organisatie of een land als er geen gepaste controles zijn ingesteld, inclusief de veiligheid van uw gegevens en andere persoonsgegevens.

### Openbaarmaking van gegevens
#### Wettelijke vereisten
Meerdaal kan uw Persoonsgegevens openbaar maken als het te goeder trouw de mening is toegedaan dat een dergelijke handeling noodzakelijk is:

- Om te voldoen aan een wettelijke verplichting
- Om de rechten en eigendom van Meerdaal te beschermen en te verdedigen
- Om mogelijke misstanden te voorkomen of te onderzoeken in verband met de Dienst
- Om de persoonlijke veiligheid van gebruikers van de Dienst of het publiek te beschermen
- Als bescherming tegen juridische aansprakelijkheid

### Veiligheid van gegevens
De veiligheid van uw gegevens is belangrijk voor ons, maar vergeet niet dat geen enkele methode van verzending via het internet of elektronische methode van opslag 100% veilig is. Hoewel wij ernaar streven commercieel aanvaardbare middelen toe te passen om uw Persoonsgegevens te beschermen, kunnen wij de absolute veiligheid niet waarborgen.

### Dienstverleners
Wij kunnen externe bedrijven en personen aanstellen om onze Dienst ("Dienstverleners") te vereenvoudigen, om de Dienst te leveren in onze naam, om diensten uit te voeren in het kader van onze Dienst of om ons te helpen bij de analyse van hoe onze Dienst wordt gebruikt.

Deze externe partijen hebben enkel toegang tot uw Persoonsgegevens om deze taken uit te voeren in onze naam en zij mogen deze niet openbaar maken aan anderen of ze gebruiken voor andere doeleinden.

### Links naar andere sites
Onze Dienst kan links bevatten naar andere sites die niet door ons worden beheerd. Als u klikt op een link van een externe partij wordt u naar de site van die externe partij gebracht. Wij raden u sterk aan het Privacybeleid te verifiÃ«ren van elke site die u bezoekt.

Wij hebben geen controle over en aanvaarden geen aansprakelijkheid met betrekking tot de inhoud, het privacybeleid of de privacypraktijken van de sites of diensten van een externe partij.

### Wijzigingen aan dit Privacybeleid
Wij kunnen ons Privacybeleid op gezette tijden bijwerken. Wij zullen u op de hoogte brengen van eventuele wijzigingen door het nieuwe Privacybeleid te publiceren op deze pagina.

Wij zullen u op de hoogte brengen via e-mail en/of een duidelijke melding op onze Dienst voor de wijzigingen van kracht gaan en wij zullen de "aanvangsdatum" bijwerken die vermeld staat bovenaan in dit Privacybeleid.

Wij raden u aan dit Privacybeleid regelmatig te controleren op eventuele wijzigingen. Wijzigingen aan dit Privacybeleid gaan van kracht op het moment dat ze worden gepubliceerd op deze pagina.

### Contact opnemen
Als u vragen hebt over dit Privacybeleid kunt u contact opnemen met ons:

- Via email: philip@dannaux-consulting.be